package com.example.habitservice.repository;

import com.example.habitservice.model.Category;
import com.example.habitservice.repository.CategoryRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.Assert;

import javax.persistence.EntityManager;
import javax.sql.DataSource;

@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
public class CategoryRepositoryTests {
    @Autowired
    private DataSource dataSource;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private CategoryRepository categoryRepository;

    @Test
    void autowiredComponentsAreNotNull() {
        Assert.notNull(dataSource, "The data source must not be null");
        Assert.notNull(jdbcTemplate, "The jdbc template must not be null");
        Assert.notNull(entityManager, "The entity manager must not be null");
        Assert.notNull(categoryRepository, "The repository must not be null");
    }

    @Test
    void whenSaved_findAllCategories() {
        categoryRepository.save(new Category("Go for a walk", "Take in the fresh air", "BOOL"));
        Assert.notNull(categoryRepository.findAll(), "The category list must not be null");
    }
}
