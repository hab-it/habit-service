package com.example.habitservice.repository;

import com.example.habitservice.model.Goal;
import com.example.habitservice.model.Progress;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.Assert;

import javax.persistence.EntityManager;
import javax.sql.DataSource;

@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
public class ProgressRepositoryTests {
    @Autowired
    private DataSource dataSource;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private ProgressRepository progressRepository;

    @Test
    void autowiredComponentsAreNotNull() {
        Assert.notNull(dataSource, "The data source must not be null");
        Assert.notNull(jdbcTemplate, "The jdbc template must not be null");
        Assert.notNull(entityManager, "The entity manager must not be null");
        Assert.notNull(progressRepository, "The repository must not be null");
    }

    @Test
    void whenSaved_getByGoal_IdAnd() {
        Goal goal = new Goal("1", "3");
        goal.setId(1L);

        progressRepository.save(new Progress(goal, "2021-05-12", "3"));
        Assert.notNull(progressRepository.findByGoal_IdAndDate(1L, "2021-05-12"), "The result must not be null");
    }
}
