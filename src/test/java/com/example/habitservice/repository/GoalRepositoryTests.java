package com.example.habitservice.repository;

import com.example.habitservice.model.Category;
import com.example.habitservice.model.Goal;
import com.example.habitservice.repository.GoalRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.Assert;

import javax.persistence.EntityManager;
import javax.sql.DataSource;

@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
public class GoalRepositoryTests {
    @Autowired
    private DataSource dataSource;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private GoalRepository goalRepository;

    @Test
    void autowiredComponentsAreNotNull() {
        Assert.notNull(dataSource, "The data source must not be null");
        Assert.notNull(jdbcTemplate, "The jdbc template must not be null");
        Assert.notNull(entityManager, "The entity manager must not be null");
        Assert.notNull(goalRepository, "The repository must not be null");
    }

    @Test
    void whenSaved_findAllGoals() {
        goalRepository.save(new Goal("3", "5"));
        Assert.notNull(goalRepository.findAll(), "The goal list must not be null");
    }

    @Test
    void whenSaved_RetrieveGoal() {
        Assert.notNull(goalRepository.save(new Goal("3", "5")), "The goal must not be null");
    }
}
