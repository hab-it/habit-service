package com.example.habitservice.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.util.Assert;

@SpringBootTest( webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CategoryControllerTests {
    @LocalServerPort
    private int port;

    @Autowired
    TestRestTemplate restTemplate;

    @Autowired
    private CategoryController categoryController;
    private HttpHeaders headers = new HttpHeaders();

    @Test
    void autowiredComponentsAreNotNull() {
        Assert.notNull(categoryController, "The controller must not be null");
    }

    @Test
    void findAllShouldReturnAllCategories () {
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange("http://localhost:" + port + "habit/category", HttpMethod.GET, entity, String.class);
        HttpStatus expected = HttpStatus.OK;

        Assert.isTrue(response.getStatusCode() == expected, "The response code must be the same as the expected (200)");
    }
}
