package com.example.habitservice.controller;

import com.example.habitservice.model.Goal;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.util.Assert;

@SpringBootTest( webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GoalControllerTests {
    @LocalServerPort
    private int port;

    @Autowired
    TestRestTemplate restTemplate;

    @Autowired
    private GoalController goalController;
    private HttpHeaders headers = new HttpHeaders();

    @Test
    void autowiredComponentsAreNotNull() {
        Assert.notNull(goalController, "The controller must not be null");
    }

    @Test
    void findAllShouldReturnAllGoals () {
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange("http://localhost:" + port + "habit/goal", HttpMethod.GET, entity, String.class);
        HttpStatus expected = HttpStatus.OK;

        Assert.isTrue(response.getStatusCode() == expected, "The response code must be the same as the expected (200)");
    }

    @Test
    void createGoal() {
        HttpEntity<Goal> entity = new HttpEntity<Goal>(new Goal("1", "3"), headers);
        ResponseEntity<String> response = restTemplate.exchange("http://localhost:" + port + "habit/goal", HttpMethod.POST, entity, String.class);
        HttpStatus expected = HttpStatus.OK;

        Assert.isTrue(response.getStatusCode() == expected, "The response code must be the same as the expected (200)");
    }

    @Test
    void createEmptyGoal() {
        HttpEntity<Goal> entity = new HttpEntity<Goal>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange("http://localhost:" + port + "habit/goal", HttpMethod.POST, entity, String.class);
        HttpStatus expected = HttpStatus.BAD_REQUEST;

        Assert.isTrue(response.getStatusCode() == expected, "The response code must be the same as the expected (400)");
    }
}
