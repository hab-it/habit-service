package com.example.habitservice.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.util.Assert;

@SpringBootTest( webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProgressControllerTests {
    @LocalServerPort
    private int port;

    @Autowired
    TestRestTemplate restTemplate;

    @Autowired
    private ProgressController progressController;
    private HttpHeaders headers = new HttpHeaders();

    @Test
    void autowiredComponentsAreNotNull() {
        Assert.notNull(progressController, "The controller must not be null");
    }

    @Test
    void findAllShouldReturnAllProgress () {
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange("http://localhost:" + port + "habit/progress/", HttpMethod.GET, entity, String.class);
        HttpStatus expected = HttpStatus.OK;

        Assert.isTrue(response.getStatusCode() == expected, "The response code must be the same as the expected (200)");
    }

    @Test
    void findProgress_ShouldCreateNew () {
        headers.add("date", "2021-05-25");
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange("http://localhost:" + port + "habit/progress/goal/2", HttpMethod.GET, entity, String.class);
        HttpStatus expected = HttpStatus.OK;

        Assert.isTrue(response.getStatusCode() == expected, "The response code must be the same as the expected (200)");
    }

    @Test
    void findProgress_ShouldReturnProgress() {
        headers.add("date", "2021-05-25");
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange("http://localhost:" + port + "habit/progress/goal/2", HttpMethod.GET, entity, String.class);
        HttpStatus expected = HttpStatus.OK;

        Assert.isTrue(response.getStatusCode() == expected, "The response code must be the same as the expected (200)");
    }
}
