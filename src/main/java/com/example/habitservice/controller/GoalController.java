package com.example.habitservice.controller;

import com.example.habitservice.model.Goal;
import com.example.habitservice.repository.GoalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/habit/goal")
public class GoalController {
    @Autowired
    private GoalRepository repository;

    @RequestMapping(method = GET)
    public List<Goal> findAll() {
        return repository.findAll();
    }

    @RequestMapping(method = POST)
    public Goal create(@RequestBody Goal goal) {
        try {
            return repository.save(goal);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }
}
