package com.example.habitservice.controller;


import com.example.habitservice.model.Category;
import com.example.habitservice.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping("/habit/category")
public class CategoryController {
    @Autowired
    private CategoryRepository repository;

    @RequestMapping(method = GET)
    public List<Category> findAll() {
        return repository.findAll();
    }
}
