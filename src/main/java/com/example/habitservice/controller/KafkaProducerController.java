package com.example.habitservice.controller;

import com.example.habitservice.model.Progress;
import com.example.habitservice.service.KafkaProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/habit/kafka")
public class KafkaProducerController {
    @Autowired
    KafkaProducerService kafkaProducerService;

    @PostMapping("/publish/json")
    public ResponseEntity<String> publishJSONMessage(@RequestBody Progress progress) {
        kafkaProducerService.sendProgress(progress);
        return new ResponseEntity<String>("Json Published Successfully", HttpStatus.OK);
    }

    @PostMapping("/publish/{name}")
    public ResponseEntity<String> publishMessage(@PathVariable(name = "name") String name) {
        kafkaProducerService.sendStringMessage(name);
        return new ResponseEntity<String>("Published Successfully", HttpStatus.OK);
    }
}
