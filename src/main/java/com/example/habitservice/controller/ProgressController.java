package com.example.habitservice.controller;


import com.example.habitservice.model.Goal;
import com.example.habitservice.model.Progress;
import com.example.habitservice.repository.ProgressRepository;
import com.example.habitservice.service.KafkaProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.PATCH;

@RestController
@RequestMapping("/habit/progress")
public class ProgressController {
    @Autowired
    private ProgressRepository repository;

    @Autowired
    KafkaProducerService kafkaProducerService;

    @RequestMapping(method = GET)
    public List<Progress> findAll() {
        return repository.findAll();
    }

    @RequestMapping(value = "/goal/{id}", method = GET)
    public Progress findByGoalId(@PathVariable("id") Long id, @RequestHeader("date") String date) {
        // Find progress, if not found create an empty one.
        try {
            Progress result =  repository.findByGoal_IdAndDate(id, date);
            if (result != null) {
                return result;
            }
            else {
                Goal emptyGoal = new Goal();
                emptyGoal.setId(id);

                Progress emptyProgress = new Progress();
                emptyProgress.setGoal(emptyGoal);
                emptyProgress.setDate(date);
                emptyProgress.setValue("0");

                return repository.save(emptyProgress);
            }
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/{id}", method = PATCH)
    public Progress update(@PathVariable("id") Long id, @RequestHeader("value") String value) {
        try {
            Optional<Progress> updatedProgress = repository.findById(id).map(progress -> {
                progress.setValue(value);

                return repository.save(progress);
            });

            updatedProgress.ifPresent(progress -> {
                if (progress.getGoal().getValue().equals(progress.getValue())) {
                    kafkaProducerService.sendProgress(progress);
                }
            });

            if (updatedProgress.isPresent()) {
                return updatedProgress.get();
            }
            else {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Sorry, the progress couldn't be updated.");
            }
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Progress could not be updated", ex);
        }
    }
}
