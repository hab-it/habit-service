package com.example.habitservice.service;

import com.example.habitservice.model.Progress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaProducerService {
    @Autowired
    private KafkaTemplate<String, Progress> kafkaProgressTemplate;
    private static final String progressTopic = "progress";

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;
    private static final String topic = "kafka";

    public void sendProgress(Progress progress) {
        kafkaProgressTemplate.send(topic, progress);
    }

    public void sendStringMessage(String name) {
        kafkaTemplate.send(topic, name);
    }
}
