package com.example.habitservice.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
@Data
@Getter
@Setter
public class Progress {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name="goalId")
    private Goal goal;

    private String date;

    @ColumnDefault("0")
    private String value;

    public Progress() { }

    public Progress(Goal goal, String date, String value) {
        this.goal = goal;
        this.date = date;
        this.value = value;
    }
}
