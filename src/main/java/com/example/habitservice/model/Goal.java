package com.example.habitservice.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
@Data
@Getter
@Setter
public class Goal {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name="habitId")
    private Category habit;

    private String userId;

    private String value;

    public Goal() { }

    public Goal(String userId, String value) {
        this.userId = userId;
        this.value = value;
    }
}
