package com.example.habitservice.repository;

import com.example.habitservice.model.Progress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProgressRepository extends JpaRepository<Progress, Long> {
    Progress findByGoal_IdAndDate(Long id, String date);
}
