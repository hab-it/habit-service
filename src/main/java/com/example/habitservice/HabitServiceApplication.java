package com.example.habitservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class HabitServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(HabitServiceApplication.class, args);
	}

}
